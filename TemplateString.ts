function radioMessage(strings: TemplateStringsArray, ...values: number[]) {
	let result = "";

	for(let i = 0; i< strings.length; i++){

		result += strings[i] + '/STOP/';

		if(i < values.length){
			result += values[i] + '/STOP/';
		}
	}

	return result;
}
/**
 * Execution
 */

let minute = 5;
let text = radioMessage`Don't move until ${minute} minutes. It is not safe.`;
console.log(text);

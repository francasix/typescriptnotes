/**
 * Array destructuring
 * @type {number}
 */
let value1 = 2;
let value2 = 4;

// equivalent method
let [value3, , value4] = [6, 8, 10];

/**
 * Object destructuring
 * @type {{val1: number; val2: number}}
 */

const object = {val1: 1, val2: 3};
let val1 = object.val1; // 1
let val2 = object.val2; // 3

// equivalent method
const otherObject = {val3: 5, val4: 7};
let {val3, val4} = otherObject; // 5 & 7

// with new typed variable name
const newObject = {value0: undefined, value1: 11};
let new0;
let new1:number;
({ value0: new0 = 13, value1: new1} = newObject);

/**
 * Execution
 */

console.log(value1, value2, value3, value4);
[value3, value4] = [value1, value2];
console.log(value1, value2, value3, value4);

console.log(new0, new1);





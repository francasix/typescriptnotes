// Declare the tuple
let option: [string, boolean];

// Correctly initialize it
option = ["uppercase", true];

// tuple with optionnal values
type Point = [number, number?, number?];
const x: Point = [10];
console.log(x.length);


/**
 * last element as rest parameter
 */

// tuple with a string element followed by any amount of number elements
type TestScores = [string, ...number[]];

const thaliaTestScore = ["Thalia", ...[100, 98, 99, 100]];
const davidTestScore = ["David", ...[100, 98, 100]];

console.log(thaliaTestScore);
console.log(davidTestScore);

/**
 * functions equivalents
 */

function example(...args: [string, boolean, number]){
	console.log(args);
};

function exampleEquivalent(args_0: string, args_1: boolean, args_2: number){
	console.log(args_0);
};

// tuple type forces us to pass a number for x, y, and z while an array could be empty
function draw(...point3D: [number, number, number]): void;

/**
 * with spread expressions
 */

type Point3D = [number, number, number];

const draw = (...point3D: Point3D) => {
	console.log(point3D);
};

const xyzCoordinate: Point3D = [10, 20, 10];

/**
 * Execution
 */

example("TypeScript example", true, 100);
exampleEquivalent("TypeScript exampleEquivalent", true, 100);

draw(10, 20, 10);
draw(xyzCoordinate[0], xyzCoordinate[1], xyzCoordinate[2]);
// with spread operator
draw(...xyzCoordinate);

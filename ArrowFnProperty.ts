namespace arrowProperty {


	class MyClass {

		arrowFunctionProperty = () => {
			console.log(this);
		};

		regularMethod() {
			console.log(this);
		}
	}

	class MyChildClass extends MyClass {

		/**
		 * Problème:
		 * parent's arrow function is not accessible
		 */
		arrowFunctionProperty = () => {
		};

		regularMethod() {
			super.regularMethod();
		}
	}

	/**
	 * Execution
	 */

	// context this is preserved on instanciation of the arrow function
	let a = new MyClass().arrowFunctionProperty.call({});

	let b = new MyClass().regularMethod.call({});

}

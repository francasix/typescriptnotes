class AvoidAny {

	method(val: string | number) {

		if (typeof val == 'number') {

			console.log('type number');

		} else if (typeof val == 'string') {

			console.log('type string');

		}
	}
}

/**
 * Execution
 */

let a = new AvoidAny;

a.method('test');
a.method(4);
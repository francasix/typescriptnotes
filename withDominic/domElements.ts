document.addEventListener('DOMContentLoaded', ()=>{

	/**
	 * explicit type HTMLInputElement of the right hand element
	 * @type {HTMLInputElement}
	 */
	const input = <HTMLInputElement> document.getElementById('input');

	/**
	 * new/other syntax
	 * @type {HTMLButtonElement}
	 */
	const button = document.getElementById('btn') as HTMLButtonElement;

	button.addEventListener('click', ()=>{

		/**
		 * no need to check type of instance any more
		 */
		//if(input instanceof HTMLInputElement){
			console.log('input', input.value);
		//}

	})
});
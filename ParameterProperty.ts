class Class {

	constructor(public param: number){

		console.log('param', param);
	}

}

class IdenticalClass {

	param: number;

	constructor(param: number){

		this.param = param;
		console.log('param', param);
	}
}

/**
 * Execution
 */

let myParam = 1;
let c = new Class(myParam);
class Parent {
	value: number = 5;

	test(this: Parent){

		console.log('test', this.value);
	}
}

/**
 * Execution
 *
 * @type {Parent}
 */

let p = new Parent();
p.test();


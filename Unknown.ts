/**
 * TypeScript 3.0 introduces a new type called unknown
 * it helps preventing error by stopping compilation
 */

let testAny: any = 10;
let testUnknown: unknown = 10;

console.log(testAny.children);
console.log(testUnknown.children);

/**
 * type predicate to narrow the type in the block guarded
 * @param loc
 */

let itemLocat: unknown = { coordinates: { x: 10, y: "cows", z: true } };

const itemLocatCheck = (loc: any): loc is { coordinates: { x: any; y: any; z: any} } => {

	return (
		!!loc &&
		typeof loc === "object" &&
		"coordinates" in loc &&
		"x" in loc.coordinates &&
		"y" in loc.coordinates
	);
};

/**
 * Execution
 */

if (itemLocatCheck(itemLocat)) {

	console.log(itemLocat.coordinates.x);
	console.log(itemLocat.coordinates.y);
	console.log(itemLocat.coordinates.z);
}
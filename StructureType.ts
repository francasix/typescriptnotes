class ClassA {

	id: number = 0;
	value: string = "default";
}

class ClassB {

	id: number = 1;

}

class ClassC {

	id: number = 2;

}

function f1(param: ClassA) {
	console.log('f1', param);
}

function f2(param: ClassB){
	console.log('f2', param);
}

/**
 * Execution
 */

f1(new ClassA());

// change default properties
f1({
	id: 6,
	value: 'new'
});

// add a new property
f1({
	id: 4,
	value: 'test',
	newProp: 3,
} as ClassA);

// B and C have the same properties
f2(new ClassC());
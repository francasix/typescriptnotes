// custom type object with optional values
type Options = {
	option1?: string;
	option2?: string;
}

class CompositionType {

	f1(val: string, options: Options){

		// options are not mandatory
		return (options.option1 || '') + val + (options.option2 || '');
	}
}

/**
 * Execution
 */

const o0: Options = {option1: "before ", option2: " after"};
const o1: Options = {option1: "before "};
let compo = new CompositionType;

console.log(compo.f1('middle', o0));
console.log(compo.f1('end', o1));
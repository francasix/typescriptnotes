class NumberHelper {
	// rest parameter
	static sumParams(...numbers: number[]){

		return numbers.reduce((a, b)=>a+b);

	}
}

/**
 * Execution
 */

let numA = NumberHelper.sumParams(1, 2);
console.log(numA);

let arr = [4, 1];
// spread operator => params passed like individual ones
let numB = NumberHelper.sumParams(...arr);
console.log(numB);

let newArr = [2,...arr];
let numC = NumberHelper.sumParams(...newArr, 3);
console.log(numC);
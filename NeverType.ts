type Podium = 1 | 2 | 3;

function running(podium: Podium) {

	switch (podium) {
		case 1:
			return "Winner";
		case 2:
			return "Second, try again";
		case 3:
			return "At least you have a medal";
		default:
			// all possibilities of podium's value have been checked
			const never: never = podium;
			console.log('test', never);
			assertNever(podium, "Looser");
	}

	return true;
}

function assertNever(value: never, errorMessage: string) {
	console.log('VALUE', value);
	throw new Error(errorMessage);
}

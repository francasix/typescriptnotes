/**
 * async / await
 */

class OlympicGames {

	async curling() {
		console.log('curling...');
		const f = await this.testFrozen();

		// will be executed when testFrozen returns
		console.log(f);

	}

	testFrozen() {
		return new Promise<string>((resolve, reject) => {

			setTimeout(() => {

				resolve('frozen test done');

			}, 1000)
		});

	}
}

/**
 * Promise
 */

class Jogger {

	training() {

		this.effortTest().then((r)=>{

			console.log(r);

		});

	}

	effortTest() {
		return new Promise<string>((resolve, reject) => {

			setTimeout(() => {

				resolve('effort test done');

			}, 1000)
		});

	}
}

/**
 * Execution
 */

// let j = new Jogger().training();
let cu = new OlympicGames().curling();

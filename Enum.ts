/**
 * enum vs const enum
 */

enum SizeWithDefinedValues {
	small = 1,
	average = 10,
	tall = 100
}

const enum List {
	first = 1,
	second,
	third
}

function assignCategory(size: SizeWithDefinedValues) {

	switch(size){
		case SizeWithDefinedValues.small:
			console.log(SizeWithDefinedValues.small);
			return "Small category!";
		case SizeWithDefinedValues.average:
			console.log(SizeWithDefinedValues.average);
			return "Average category!";
		case SizeWithDefinedValues.tall:
			console.log(SizeWithDefinedValues.tall);
			return "Tall category!"
		default:
			return "No category"
	}

}


function pickItem(item: List) {

	switch(item){
		case List.first:
			console.log(List.first);
			return "First!";
		case List.third:
			console.log(List.third);
			return "Third!"
		default:
			return "No item picked"
	}

}

/**
 * Execution
 */

console.log(assignCategory(SizeWithDefinedValues.small));
console.log(assignCategory(SizeWithDefinedValues.tall));
console.log(pickItem(List.third));
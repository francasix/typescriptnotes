// const values: Array<number> = [];
// const items: [string, number] = ['test', 8];

type Firefox = {
	firefox: string;
	modules?: [string, string];
}

type Chrome = {
	chrome: string;
	extensions?: Array<string>;
}

// both browsers
function MacOS(browser: Chrome & Firefox) {

	return `Browsers installed: Firefox ${browser.firefox} & Chrome ${browser.chrome} with modules ${browser.modules[0]} and ${browser.modules[1]}`

}

/**
 * Execution
 */

console.log(MacOS({
	firefox: '60.1',
	chrome: '66.4',
	modules: ['downloadherlper', 'measureit']
}));
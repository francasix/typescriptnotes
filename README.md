# Typescript memory / helper

## Test

* compile file.ts into filter.js
* `node file.js`

## Based on

[Udemy course](https://www.udemy.com/mastering-typescript-second-edition/learn/v4/t/lecture/9028598?start=0)

[Exploring tuple & unknown type](https://auth0.com/blog/typescript-3-exploring-tuples-the-unknown-type/)
interface SmallElement {
	smell: string;
	quantity: number;
}

interface BigElement {
	texture: string;
	quantity: number;
}

interface Content extends SmallElement, BigElement{
	type: string;
}

interface Box extends Content {
	color: string;
	material: string;
}


/**
 * Execution
 *
 * @type {{color: string; material: string; type: string; quantity: number; smell: string; texture: string}}
 */

let myBox: Box = {
	color: 'blue',
	material: 'paper',
	type: 'gold',
	quantity: 5,
	smell: 'good',
	texture: 'dry'
};

function makeAGift(box: Box) {
	console.log(box.type);
}

makeAGift(myBox);
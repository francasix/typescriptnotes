/**
 * keyof operator
 */

interface Classroom {
	door: number;
	chairs: number;
	windows: number;
	teacher?: string;
}

// secure key syntax
let keyOfClassroom: keyof Classroom = 'teacher';

/**
 * Mapped types
 *
 * original keys
 * correct value's type
 * readonly option
 */

type readOnlyClassroom = {
	readonly [key in keyof Classroom]: Classroom[key];
}

let myReadOnlyClassroom: readOnlyClassroom = {
	door: 2,
	chairs: 33,
	windows: 2,
	teacher: 'Madame Tuche'
};

/**
 * Mapped types
 *
 * generic way
 *
 */

interface House {
	windows: number;
	address?: string;
}

type myReadOnly<TYPE> = {
	readonly [key in keyof TYPE]: TYPE[key];
	// equivalent
	//readonly [key in keyof TYPE]: number | string;
}

let myReadOnlyHouse: myReadOnly<House> = {
	windows: 5
}

/**
 * Example
 */

interface Carpet {
	persian: boolean;
	berber: boolean;
	saler: string;
}

type origin<TYPE> = {
	readonly [key in keyof TYPE]?: TYPE[key];
}

let carpets: origin<Carpet> = {
	persian: true,
	berber: false,
	saler: 'Le Pape'
};

// READONLY :O)
// carpets.persian = false;
// carpets.saler = 'sa femme';

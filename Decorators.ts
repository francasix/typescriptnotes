/**
 * Decorator simple
 *
 * @param {Object} target
 * @param {string} method
 * @param {TypedPropertyDescriptor<any>} descriptor
 * @returns {TypedPropertyDescriptor<any>}
 */

function announcementDecorator(target: Object, method: string, descriptor: TypedPropertyDescriptor<any>) {

	const originalMethod = descriptor.value;

	descriptor.value = function (...args: any[]) {

		const returnedValue = originalMethod.apply(this, args);

		console.log('Dear customers...', returnedValue);
	};

	return descriptor;
}

/**
 * Decorator inside function
 *
 * @param {Converter} converter
 * @returns {(target: Object, method: string, descriptor: TypedPropertyDescriptor<any>) => void}
 */

function updateScreenDecorator(converter: Converter) {


	return (target: Object, method: string, descriptor: TypedPropertyDescriptor<any>) => {

		const originalMethod = descriptor.value;

		descriptor.value = function (...args: any[]) {

			const returnedValue = originalMethod.apply(this, args);

			let price = converter.inEuros(returnedValue);
			console.log('The price is', price);
		};

	}
}


/**
 * methods example with decorator
 */

class Supermarket {

	@announcementDecorator
	takeBasket(){
		return ['apples', 'bananas']
	}

	@announcementDecorator
	enter(){
		return 'Micheline just entered';
	}

	@updateScreenDecorator(inEuro)
	payCash(){
		return 12;
	}

	@updateScreenDecorator(inEuro)
	payCredit(){
		return 15;
	}
}

/**
 * Class as decorator's argument
 */

class Converter {

	inEuros(amount: number){
		return `${amount}€`;
	}
}

let inEuro = new Converter();

/**
 * Execution
 *
 */

let newCustomer = new Supermarket().takeBasket();
let enterer = new Supermarket().enter();
let customer1 = new Supermarket().payCash();
let customer2 = new Supermarket().payCredit();

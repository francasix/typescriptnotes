class Home {

	constructor() {

	}

	clean(something: string | undefined) {

		// avoid error when argument is undefined or null
		return something == null || `${something!.toLowerCase()} has been cleaned`;

	}
}

/**
 * Execution
 *
 */

let h = new Home();
console.log(h.clean('kitchen'));
console.log(h.clean(undefined));
// console.log(h.clean());
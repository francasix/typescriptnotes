function* exampleFn() {
	console.log('1');
	yield 2;
	let nextValue = yield 3;
	console.log(nextValue);
}

/**
 * Execution of exampleFn()
 * @type {IterableIterator<number>}
 */

const g = exampleFn();
console.log('0');
console.log(g.next().value);
console.log(g.next().value);
g.next(4);
console.log('END', g.next());


/**
 * loop through generator
 * @returns {IterableIterator<string>}
 */

function* generator() {
	for (let i = 0; i < 100; i++){

		yield `yield ${i}`;

	}
}
for(let x of generator()){
	console.log(x);
}
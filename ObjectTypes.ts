const obj0: {} = {prop: 5};
const obj1: {} = 3;
const obj2: {} = "test2";
const obj3: Object = "test3";
const obj4: Object = {piou: 'piou'};


/**
 * Execution
 */

let mustBeObject: object = obj2;

console.log(obj0['prop'], obj1, obj2, obj3);
console.log(typeof obj0, typeof obj1, typeof obj2, typeof obj3);
console.log(obj4.hasOwnProperty('piou'));
